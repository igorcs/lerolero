# Gerador de Lero-Lero
import random

parte1 = ["O sistema em desenvolvimento",
          "O novo protocolo de comunicação",
          "O algoritmo de otimização"]
parte2 = ["possui excelente desempenho",
          "oferece garantias de precisão acima da média",
          "preenche uma lacuna significativa"]
parte3 = ["nas aplicações a que se destina",
          "em relação 'as opções disponíveis no mercado",
          "\r, provendo ampla vantagem competitiva a seus usuários"]

lingua = int(input("Escolha a língua: 1 - português; 2 - inglês \n"))

if lingua == 2:
    parte1 = ["Good morning"]
    parte2 = ["Good afternoon"]
    parte3 = ["Good night"]
    

print(random.choice(parte1), random.choice(parte2), random.choice(parte3))

